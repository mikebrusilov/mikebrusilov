package models

type ResumeResponse struct {
	Spec struct {
		Name      string `json:"name"`
		About     string `json:"about"`
		Github    string `json:"github"`
		LinkedIn  string `json:"linkedin"`
		Education []struct {
			University     string `json:"university"`
			College        string `json:"college"`
			Certifications string `json:"certifications"`
		}
		Experience []struct {
			CurrentRole             string `json:"currentRole"`
			CurrentRoleDates        string `json:"currentRoleDates"`
			CurrentRoleDescription  string `json:"currentRoleDescription"`
			PreviousRole            string `json:"previousRole"`
			PreviousRoleDates       string `json:"previousRoleDates"`
			PreviousRoleDescription string `json:"previousRoleDescription"`
		}
		Technologies []struct {
			Infrastructure string `json:"infrastructure"`
			Programming    string `json:"programming"`
			Other          string `json:"other"`
		}
		OpenSourceContributions []struct {
			CovidShield string `json:"covidShield"`
		}
	}
}
