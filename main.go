package main

import (
	"fmt"
	"mkbru/k8s-resume/k8s"
	"mkbru/k8s-resume/models"
	"mkbru/k8s-resume/utils"
	"os"
)

func run() error {
	var fileName = "mike-brusilovsky-resume"

	if err := k8s.ApplyCRDs(); err != nil {
		return err
	}

	if err := k8s.ApplyYAMLs(); err != nil {
		return err
	}

	if err := k8s.GetYAMLJsonOutput(fileName); err != nil {
		return err
	}

	var resume models.ResumeResponse
	if err := k8s.ParseJsonFile(fileName, &resume); err != nil {
		return err
	}

	utils.PrettyPrint(resume)

	return nil
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
