## Kubernetes Resume

This is a fun little project I made to showcase some of my Go and Kubernetes abilities.

As mentioned above, this is a "fun little project". I have not included unit tests and some values are hardcoded!

I wanted present my resume in a relevant way.

## What's happening in `main.go`?

1. A CRD for a `KubernetesResume` is defined and applied to the cluster.
2. `myresume.yaml` of `kind: KubernetesResume` is applied to the cluster.
3. The custom resource is then exported to a json file.  
4. The json file is parsed, and relevant resume info is printed to the screen.

## Pre-reqs

1. Have a running kubernetes cluster (I used a kind cluster)
2. Have a golang runtime installed
3. Have git installed

## Running The Code

```
git clone https://gitlab.com/mikebrusilov/mikebrusilov.git
cd k8s-resume
go run main.go
```

## Expected Output

My resume in a json format.