package k8s

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
)

func ApplyCRDs() error {
	kubectlRun := exec.Command("kubectl", "apply", "-f", "./yamls/crd.yaml")
	kubectlRun.Stdin = os.Stdin
	kubectlRun.Stdout = os.Stdout
	kubectlRun.Stderr = os.Stderr

	if err := kubectlRun.Run(); err != nil {
		return err
	}

	return nil
}

func ApplyYAMLs() error {
	kubectlRun := exec.Command("kubectl", "apply", "-f", "./yamls/myresume.yaml")
	kubectlRun.Stdin = os.Stdin
	kubectlRun.Stdout = os.Stdout
	kubectlRun.Stderr = os.Stderr

	if err := kubectlRun.Run(); err != nil {
		return err
	}

	return nil
}

func GetYAMLJsonOutput(fileName string) error {

	kubectlRun := exec.Command("kubectl", "get", "kr", "mike-brusilovsky-resume", "-o", "json")
	fullFileName := fmt.Sprintf("./%s.json", fileName)
	jsonFile, err := os.Create(fullFileName)
	if err != nil {
		panic(err)
	}
	defer jsonFile.Close()
	kubectlRun.Stdout = jsonFile
	kubectlRun.Stdin = os.Stdin
	kubectlRun.Stderr = os.Stderr

	if err := kubectlRun.Run(); err != nil {
		return err
	}

	return nil
}

func ParseJsonFile(fileName string, output interface{}) error {
	fullFileName := fmt.Sprintf("./%s.json", fileName)
	jsonFile, err := os.Open(fullFileName)
	if err != nil {
		return fmt.Errorf("could not parse json file %q: %w", fileName, err)
	}

	if err := json.NewDecoder(jsonFile).Decode(&output); err != nil {
		return fmt.Errorf("could not decode json file %q: %w", fileName, err)
	}

	return nil
}
